

import Foundation
import UIKit

public struct MealPlan{
    public var image: UIImage
    public var meal: String
    public var dishName: String
}

enum Tag{
    
    static let breakfast: String = "Breakfast"
    static let morningSnack: String = "MorningSnack"
    static let lunch: String = "Lunch"
    static let afternoonSnack: String = "AfternoonSnack"
    static let dinner: String = "Dinner"
}

extension MealPlan{
    public static func allFoodCells() -> [MealPlan] {
        
        let breakfast = MealPlan(image: #imageLiteral(resourceName: "image2"), meal: Tag.breakfast, dishName: "Veggie Omlet")
        let morningSnack = MealPlan(image: #imageLiteral(resourceName: "image1"), meal: Tag.morningSnack, dishName: "Veggie Omlet")
        let lunch = MealPlan(image: #imageLiteral(resourceName: "image2"), meal: Tag.lunch, dishName: "Veggie Omlet")
        let afternoonSnack = MealPlan(image: #imageLiteral(resourceName: "image1"), meal: Tag.afternoonSnack, dishName: "Veggie Omlet")
        let dinner = MealPlan(image: #imageLiteral(resourceName: "image2"), meal: Tag.dinner, dishName: "Veggie Omlet")
        
        return [breakfast,morningSnack,lunch,afternoonSnack,dinner]
    }
}
