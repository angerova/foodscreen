
import UIKit

class MealPlanTableViewCell: UITableViewCell {

    @IBOutlet weak var dishName: UILabel!
    @IBOutlet weak var mealLabel: UILabel!
    @IBOutlet weak var mealImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(foodCell: MealPlan){
        dishName.text = foodCell.dishName
        mealLabel.text = foodCell.meal
        mealImageView.image = foodCell.image
        mealImageView.layer.cornerRadius = 29.0
        mealImageView.clipsToBounds = true
        contentView.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
       

    }
}

