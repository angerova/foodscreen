

import Foundation
import UIKit
import JTAppleCalendar

class CalendarCell: JTAppleCell{
   
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var todayView: UIView!
    
    func configure(today: Date, cellDate: Date, selectedDate: Date, cellState : CellState ){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        let todayStr = formatter.string(from: today)
        let cellDateStr = formatter.string(from: cellDate)
        let selectedDateStr = formatter.string(from: selectedDate)
        dateLabel.text = cellState.text
        
//            (todayStr == cellDateStr) ? "Today" :
        
        if todayStr == cellDateStr{
            todayView.isHidden = false
            todayView.layer.masksToBounds = false
            todayView.layer.cornerRadius = 17
            todayView.backgroundColor = (selectedDateStr == cellDateStr) ? Colors.colorToday: Colors.colorNotSelected
            todayView.layer.borderWidth = 0
            dateLabel.textColor = (selectedDateStr == cellDateStr) ? .white: Colors.colorNotSelected
        }else{
            todayView.isHidden = true
            dateLabel.textColor = (selectedDateStr == cellDateStr) ? Colors.colorNotToday: Colors.colorNotSelected
        }
        
        
//        if selectedDateStr == cellDateStr {
//
//            todayView.backgroundColor = (todayStr == cellDateStr) ? colorToday: colorNotToday
//            dateLabel.textColor = (todayStr == cellDateStr) ? colorToday : colorNotToday
//
//        }else{
//            dateLabel.textColor = #colorLiteral(red: 0.8389356202, green: 0.8389356202, blue: 0.8389356202, alpha: 1)
//
//            todayView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        }
    
    }
}
