

import UIKit
import JTAppleCalendar


class MealPlanViewController: UIViewController {
    
    @IBOutlet weak var calendarPeriodLabel: UILabel!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 7))
            tableView.tableFooterView?.backgroundColor = .clear
            tableView.backgroundColor = Colors.tableViewBackgroundColor
            
        }
    }
    var selectedDate: Date = Date()
    let formatter = DateFormatter()
    var startDate: Date!
    var endDate: Date!
    var foodCells: [MealPlan] = []
    var firstUserDay: Date!
    
    @IBAction func switchDayBack(_ sender: Any) {
        
        formatter.dateFormat = "yyyy MM dd"
        if formatter.string(from: startDate) != formatter.string(from: firstUserDay){
           switchDays(componentDay: -7, newSelectedDate: addDayComponent(dayComponent: -7, toDay: startDate))
        }
        
    }
    
    @IBAction func switchDayForward(_ sender: Any) {
        switchDays(componentDay: 7, newSelectedDate: addDayComponent(dayComponent: 7, toDay: startDate))
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        foodCells = MealPlan.allFoodCells()
        tableView.dataSource = self
        tableView.delegate = self
        setUpCalendar()
        createRecognizer()
    }
    
    func setUpCalendar(){
        startDate = Date()
        
        var component = DateComponents()
        
        let weekDay = Calendar.current.component(.weekday, from: startDate)
        component.day = 1 - weekDay
        startDate = Calendar.current.date(byAdding: component, to: startDate)

        print(weekDay)
        endDate = addDayComponent(dayComponent: 6, toDay: startDate)
        calendarView.scrollToDate(startDate, triggerScrollToDateDelegate: true, animateScroll: false, preferredScrollPosition: nil, extraAddedOffset: 0, completionHandler: nil)
        calendarView.selectDates([Date()])
        formatter.dateFormat = "MMM dd"
        calendarPeriodLabel.text = "\(formatter.string(from: startDate)) - \(formatter.string(from: endDate))"
    }
    
    func createRecognizer(){
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        if gesture.direction == UISwipeGestureRecognizer.Direction.right {
            swipe(day: Dates.back, checkDate: startDate, rowAnimation: .right)
        }
        else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
            swipe(day: Dates.forward, checkDate: endDate, rowAnimation: .left)
        }
        
    }
    

    func switchDays(componentDay: Int, newSelectedDate: Date){
        startDate = addDayComponent(dayComponent: componentDay, toDay: startDate)
        endDate = addDayComponent(dayComponent: 6, toDay: startDate)
        selectedDate = newSelectedDate
        calendarView.reloadData()
        calendarView.scrollToDate(startDate, triggerScrollToDateDelegate: true, animateScroll: false, preferredScrollPosition: nil, extraAddedOffset: 0, completionHandler: nil)
        calendarView.selectDates([startDate])
        formatter.dateFormat = "MMM dd"
        calendarPeriodLabel.text = "\(formatter.string(from: startDate)) - \(formatter.string(from: endDate))"
//        here will be tableView.reloadData()
    }
    
    func swipe(day: Int, checkDate: Date, rowAnimation: UITableView.RowAnimation){
        formatter.dateFormat = "yyyy MM dd"
        
        if formatter.string(from: selectedDate) == formatter.string(from: checkDate){
            switchDays(componentDay: 7*day, newSelectedDate: addDayComponent(dayComponent: 1*day, toDay: selectedDate))
        }else{
            selectedDate = addDayComponent(dayComponent: 1*day, toDay: selectedDate)
            calendarView.reloadData()
            calendarView.selectDates([selectedDate])
            
        }
        
        let range = NSMakeRange(0, self.tableView.numberOfSections)
        let sections = NSIndexSet(indexesIn: range)
        self.tableView.reloadSections(sections as IndexSet, with: rowAnimation)
    
        
    }

}


extension MealPlanViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return foodCells.count
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell", for: indexPath) as! MealPlanTableViewCell
        cell.configure(foodCell: foodCells[indexPath.row])
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}

extension MealPlanViewController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource{
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
      
    }
    
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            firstUserDay = formatter.date(from:  UserDefaults.standard.string(forKey: "firstDay") ?? "2017 01 01")!
            
        } else {
            var component = DateComponents()
            firstUserDay = Date()
            let weekDay = Calendar.current.component(.weekday, from: firstUserDay)
            component.day = 1 - weekDay
            firstUserDay = Calendar.current.date(byAdding: component, to: firstUserDay)
//            print(firstUserDay)
            print(firstUserDay)
            UserDefaults.standard.set(formatter.string(from: Date()), forKey: "firstDay")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
        }
       print("firstUserDay")

        let parameters = ConfigurationParameters(startDate: firstUserDay, endDate: formatter.date(from: "2025 01 01")!, numberOfRows: 1, calendar: Calendar.current, generateInDates: .forFirstMonthOnly, generateOutDates: .tillEndOfRow, firstDayOfWeek: .sunday, hasStrictBoundaries: nil)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "calendarCell", for: indexPath) as! CalendarCell
        cell.configure(today: Date(), cellDate: date, selectedDate: selectedDate, cellState: cellState)
        return cell

    }
    

    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState){
        guard let cell = cell as? CalendarCell else { return }
        formatter.dateFormat = "yyyy MM dd"
        let todayStr = formatter.string(from: Date())
        let cellDateStr = formatter.string(from: date)
        cell.todayView.backgroundColor = Colors.colorNotSelected
        cell.dateLabel.textColor = (todayStr == cellDateStr) ? .white : Colors.colorNotSelected
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState){
        guard let cell = cell as? CalendarCell else { return }
        formatter.dateFormat = "yyyy MM dd"
        let todayStr = formatter.string(from: Date())
        let cellDateStr = formatter.string(from: date)
        cell.dateLabel.textColor = (todayStr == cellDateStr) ? .white : Colors.colorNotToday
        cell.todayView.backgroundColor = (todayStr == cellDateStr) ? Colors.colorToday: Colors.colorNotSelected
        selectedDate = date 
    }
    
    
}
