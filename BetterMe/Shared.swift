//
//  Shared.swift
//  BetterMe
//
//  Created by Алина Ангерова on 11/03/2019.
//  Copyright © 2019 Алина Ангерова. All rights reserved.
//

import Foundation
import UIKit


enum Dates{
    static let back: Int = -1
    static let forward: Int = 1
}
enum Colors {
    static let colorToday: UIColor = #colorLiteral(red: 0.9773638983, green: 0.3706966746, blue: 0.6539585737, alpha: 1)
    static let colorNotToday: UIColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    static let colorNotSelected: UIColor = #colorLiteral(red: 0.8389356202, green: 0.8389356202, blue: 0.8389356202, alpha: 1)
    static let tableViewBackgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
}

func addDayComponent(dayComponent: Int, toDay: Date)-> Date{
    var component = DateComponents()
    component.day = dayComponent
    let newDay = Calendar.current.date(byAdding: component, to: toDay)
    return newDay!
}
